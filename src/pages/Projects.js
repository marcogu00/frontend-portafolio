import React from "react";
import ProjectsWrapper from '../components/projects/ProjectsWrapper.js';

function Projects() {

    return (
        <div className="Projects">
            <ProjectsWrapper />
        </div>
    );
}

export default Projects;
