import React from "react";
import ContactForm from '../components/contact/ContactForm.js';
import HeroImage from '../components/global/heroImage/HeroImage';

function Contact() {

    return (
        <div className="contact-page">

            <HeroImage showImage={false} className="small bg-dark" >
                <h2 className="mb-0">
                    Contacto
                </h2>
            </HeroImage>

            <div className="container  py-5 my-5">
                <div className="row">
                    <div className="col-lg-8 col-md-10 col-12 mx-auto">
                        <ContactForm />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Contact;
