import React from "react";
import AddNewProjectForm from '../components/admin/AddNewProjectForm';
import LoginForm from '../components/admin/LoginForm';

function Admin() {

    return (
        <div className="admin py-5 my-5">
            <div className="container">
                
                <AddNewProjectForm />

                <h4 className="my-5">
                    Login: 
                </h4>

                <LoginForm />
                
            </div>
        </div>
    );
}

export default Admin;
