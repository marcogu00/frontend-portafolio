import React from "react";
import MainBanner from '../components/home/MainBanner';
import FeaturedPortfolioItems from '../components/home/FeaturedPortfolioItems';
import ProfileSection from '../components/home/ProfileSection';

function Home() {

    return (
        <div className="Home">
            <MainBanner />

            <FeaturedPortfolioItems />
            
            <ProfileSection />
        </div>
    );
}

export default Home;
