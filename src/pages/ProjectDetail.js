import React from "react";
import { withRouter } from "react-router";
import Client from '../utils/Client';
import Loader from '../components/global/loaders/Loader';

import  HeroImage from '../components/global/heroImage/HeroImage';
import ResponsiveImage from '../components/global/ResponsiveImage';
import { ReactComponent as Link } from '../assets/link.svg';

class ProjectDetail extends React.Component {
    
    constructor(props){
        super(props);
        
        this.state = { 
            id: null,
            loading: true,
            project: null
        }
    }

    componentDidMount() {
        const id = this.props.match.params.id;

        this.setState({loading: true});

        Client.get('/api/projects/'+id)
        .then(response => {
            
            response.data.data.gallery.sort((a, b) => a.order - b.order);

            this.setState({project: response.data.data, loading: false});
            
        }).catch(error => {
            this.setState({loading: false});
            console.log(error);
        })
    }

    render(){

        const { project } = this.state;

        if(this.state.loading){
            return ( <Loader width="100%" height="60vh" /> )
        }

        return (
            <div className="project-detail pb-5" style={{backgroundColor: '#f9f9f9'}} >
                
                <HeroImage img={project.img} >
                    <h2>
                        {project.title} 
                    </h2>

                    <p className="short-desc mb-5">
                        {project.short_desc} 
                    </p>

                    <div className="d-flex flex-wrap justify-content-between align-items-end">
                        <div className="mb-3 mb-md-0 project-technologies">
                            <h3> Tecnologias </h3>
                            { project.technologies.map(elm => (
                                <span className="mr-3" key={elm} >{elm}</span> 
                            )) }
                        </div>
                        { project.designer && (
                            <div className="mb-3 mb-md-0 project-designer">
                                <h3>Diseñador </h3>
                                <a href={project.designer_portfolio} className="align-items-center d-flex" target="_blank" rel="noopener noreferrer">
                                    { project.designer } 
                                    <Link className="ml-3" />
                                </a>
                            </div>
                        )}
                        <div className="mb-3 mb-md-0">
                            <h3>Año </h3>
                            <span>
                                { project.year }
                            </span>
                        </div>
                        <div className="mb-3 mb-md-0">
                            <a href={ project.url } className="align-items-center d-flex" target="_blank" rel="noopener noreferrer">
                                Visitar Sitio
                                <Link className="ml-3" />
                            </a>
                        </div>
                    </div>
                </HeroImage>

                <div className="container  pt-5 mt-5">
                    <div className="px-2 col-xl-10 mx-auto d-flex flex-wrap justify-content-center">     
                        { this.state.project.gallery.map(img => (
                            <ResponsiveImage img={img} lazy={true} className="mb-5 pb-4 px-md-3" key={img.order}/> 
                        ))}
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(ProjectDetail);
