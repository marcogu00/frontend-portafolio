import React from 'react';

export default function SplitText(props){
    
    let delay = props.delay || 0.02, accIndex = 0, result = '';

    result = props.text.split(/(\S+)/gi).map( (word, i) => {
        
        if(word === ' ') return ' ';

        return (
            <div key={word + i} >
                {word.split('').map( (match, index) => {
                    
                    accIndex++
                    return (  
                        <span 
                            key={ index + match }
                            style={{ transitionDelay: (accIndex * delay) + 's' }}
                        >
                            { (match === ' ') ? '  ' : match }
                        </span>
                    )
                })}
            </div>
        )
    });

    return (
        <React.Fragment> { result } </React.Fragment>
    );
}