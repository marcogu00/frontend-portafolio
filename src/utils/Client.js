import axios from 'axios';
import config from './config';

const Client = axios.create({
    baseURL: config.URL_API,
    withCredentials: true
});

export default Client;