const config = {
    URL_API: process.env.REACT_APP_URL_API,
    EMAIL: process.env.REACT_APP_EMAIL,
    PHONE: process.env.REACT_APP_PHONE,
    LINKEDIN: process.env.REACT_APP_LINKEDIN
}

export default config