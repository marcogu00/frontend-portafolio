const lazyImageObserver = new IntersectionObserver(function(entries, observer) {

    const width = window.innerWidth;

    entries.forEach(function(entry) {
        if (entry.isIntersecting) {
            let lazyImage = entry.target, sources = [];

            [...entry.target.querySelectorAll('source')].forEach(s => {
                sources.push({
                    url: s.dataset.srcset,
                    type: s.type,
                    media: s.media ? s.media.replace(/[:()\D]/gim, '') : 0
                });
            })

            sources.sort((a, b) =>  b.media - a.media );

            for(let s of sources){
                if( width > s.media && s.url !== undefined){
                    lazyImage.querySelector('img').src = s.url;
                    break;
                }
            }

            lazyImageObserver.unobserve(lazyImage);
        }
    });
}, {
    rootMargin: "0px 0px 256px 0px"
});

const observeNewImage = function(img){
    lazyImageObserver.observe(img);
}

export default observeNewImage