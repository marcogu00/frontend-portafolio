import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from './pages/Home';
import Projects from './pages/Projects';
import Contact from './pages/Contact';
import ProjectDetail from './pages/ProjectDetail';
import Admin from './pages/Admin';

import Header from './components/global/header/Header';
import Footer from './components/global/footer/Footer';

import ScrollToTop from './utils/ScrollToTop';
// import SmoothScroll from './utils/SmoothScroll';

function App() {
    return (
		<Router>
			<ScrollToTop />
			<Header />
			{/* <SmoothScroll >
			</SmoothScroll> */}
				<Switch>
					<Route exact path="/">
						<Home />
					</Route>

					<Route exact path="/portafolio">
						<Projects />
					</Route>
					
					<Route exact path="/proyecto/:id">
						<ProjectDetail />
					</Route>
					
					<Route exact path="/contacto">
						<Contact />
					</Route>
					
					<Route exact path="/admin">
						<Admin />
					</Route>

					<Route path="*">
						{/* <NoMatch /> */}
						<div className="py-5">
							Not Found :(
						</div>
					</Route>

				</Switch>
				<Footer />
		</Router>
    );
}

export default App;
