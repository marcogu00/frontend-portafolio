import React  from "react";
import { Link } from 'react-router-dom';
import Client from '../../utils/Client';
import config from '../../utils/config';
import './FeaturedPortfolioItems.scss';
import Loader from '../global/loaders/Loader';
import AnimatedImage from '../global/AnimatedImage';
import ScrollAnimation from 'react-animate-on-scroll';


class FeaturedPortfolioItems extends React.Component {

    constructor(props){
        super(props);
        this.state = { loading: false, items: []  }
    }

    componentDidMount(){
        
        this.setState({loading: true});
        
        Client.get('/api/projects?featured=true')
        .then(response => {

            this.setState({items: response.data.data, loading: false});
            
        }).catch(error => {
            this.setState({loading: false});
            console.log(error);
        })
    }

    PortfolioItems(){
        return (
            this.state.items.map(project => (         
                <div className="portfolio-item" key={project._id}>
                    <Link to={`/proyecto/${project._id}`}>
                        <AnimatedImage 
                            image1={`${config.URL_API}${project.img.desktop}`} 
                            image2={`${config.URL_API}${project.secondary_img.desktop}`} 
                        />

                        <ScrollAnimation animateIn="fadeUp" animateOnce={true} offset={50}>	
                            <h3>{project.title}</h3>
                        </ScrollAnimation>
                    </Link>
                </div>
            ))
        )
    }

    render(){
        return (
            <div className="section featured-portfolio-items">
                <div className="container">
                    <ScrollAnimation animateIn="fadeUp" animateOnce={true}>	
                        <h2 className="mg-heading">
                            Proyectos Destacados
                        </h2>
                    </ScrollAnimation>

                    <ScrollAnimation animateIn="fadeIn" animateOnce={true} delay={150}>	
                        <p className="mg-subheading">
                            Explora algunos de mis mejores trabajos.
                        </p>
                    </ScrollAnimation>
                    

                    <div className="portfolio-grid">

                        {this.state.loading && (
                            <>
                            <Loader className="portfolio-item" width="80%" height="450px" />
                            <Loader className="portfolio-item" width="80%" height="450px" />
                            <Loader className="portfolio-item" width="80%" height="450px" />
                            </>
                        )}
                        
                        { (this.state.items.length && !this.state.loading) ? ( this.PortfolioItems()) : ''}

                    </div>

                    <div className="d-flex justify-content-center mt-5 pt-4">
                        <Link to="/portafolio" className="btn dark">
                            Ver Más
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}

export default FeaturedPortfolioItems;