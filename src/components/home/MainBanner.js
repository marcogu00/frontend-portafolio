import React  from "react";
import { Link } from 'react-router-dom';
import './MainBanner.scss';
import SplitText from '../../utils/SplitText';
import { ReactComponent as Dots } from '../../assets/dots.svg';
import { ReactComponent as Circle } from '../../assets/circle.svg';


class MainBanner extends React.Component {

    constructor(props){
        super(props);
        this.container = React.createRef();
        this.state ={ 
            isMounted: false, 
            offset: 0,
            observer: new IntersectionObserver(this.callback, {
                root: null,
                rootMargin: '0px',
                threshold: 0.1
            })
        }
    }

    callback = (entries, observer) => {
        entries.forEach(element => {
            
            if(!element.isIntersecting){
                document.removeEventListener('scroll', this.handleScroll);
            }else{
                document.addEventListener('scroll', this.handleScroll);
            }
        });
    }

    handleScroll = (event) => {
        let top = this.container.current.getBoundingClientRect().top;
        this.setState({offset: top});
    }

    componentDidMount(){

        this.state.observer.observe(this.container.current);

        setTimeout(() => {
            this.setState({isMounted: true});
        }, 750);   
    }

    componentWillUnmount(){
        document.removeEventListener('scroll', this.handleScroll);
        this.state.observer.disconnect();
    }

    render(){
        return (
            <div className={'MainBanner ' + (this.state.isMounted ? 'mounted' : '')} ref={this.container}>
                <div className="container d-flex align-items-center h-100 position-relative">
    
                    <Dots className="dots-1" style={{
                        transform:  `translateY( ${this.state.offset * 0.1}px)`
                    }}/>

                    <div className="text-box">
                        
                        <h1 className={'faded-chars-effect ' + (this.state.isMounted ? 'active' : '')}>
                            <SplitText text="MARCO GUTIERREZ" />
                        </h1>

                        <h2 className={'mb-0 chars-effect ' + (this.state.isMounted ? 'active' : '')}>
                            <SplitText text="Desarrollador Js Full-Stack" />
                        </h2>

                        <Link to="/portafolio" className="btn light mt-4">
                            Ver más
                        </Link>
                    </div>

                    <p className="xl-text" style={{transform:  'rotate(-90deg) translateX('+ (this.state.offset * 0.3) + 'px)' }}>
                        JS
                    </p>

                    <Dots className="dots-2 d-none d-md-block" style={{
                        transform:  `translateY( ${this.state.offset * 0.3}px)`
                    }}/>
    
                    <a href="/" className="banner-scroll-link"> 
                        SCROLL
                    </a>

                    <Circle className="animated-circle" />
                </div>
            </div>
        );
    }
}

export default MainBanner;
