import React from "react";
import './ProfileSection.scss';
import ScrollAnimation from 'react-animate-on-scroll';

function ProfileSection() {

	const data = [
		{
			id: '1',
			title: 'Reign',
			start: '2020',
			finish: 'Actualidad',
			description: `
				Desarrollador full-stack, principalmente enfocado en backend, creando APIs y servicios usando el
				framework NestJS
			`,
		},
		{
			id: '2',
			title: 'Samurai E-commerce',
			start: '2018',
			finish: '2020',
			description: `
				Desarrollador de tiendas en linea usando el cms Samurai . Líder del equipo de front-end, 
				encargado de planificar las tareas del equipo, reestructurar y mejorar constantemente del proceso de 
				checkout y el modulo de mi cuenta de la plataforma ademas de reestructurar la arquitectura de las 
				tiendas que desarrollamos para agilizar la implementación de las mismas utilizando VueJs y Laravel.
			`,
		},
		{
			id: '3',
			title: 'Freelancer',
			start: '2016',
			finish: '2018',
			description: `
				Desarrollando sitios web usando php, javascript, html, css y mysql, además del uso de
				preprocesadores css como sass, utilizando librerías como Bootstrap 4 y Jquery.
				Especializado en Wordpress. Implementando pasarelas de pago en sitios e-commerce como
				WebPayPlus de Transbank, PayPal y Stripe. Nociones básicas de SEO, y uso de Git.
			`,
		}
	]
	
	const content = data.map(elm => 
		<div key={`prf-${elm.id}`} className="experience-item w-100">
			<ScrollAnimation animateIn="fadeLeft" animateOnce={true}>
				<h3>
					{ elm.title }
				</h3>
			</ScrollAnimation>

			<ScrollAnimation className="years mb-lg-0" animateIn="fadeLeft" animateOnce={true} delay={100}>
				<span>
					{ elm.start }
				</span>
				<span>
					{ elm.finish }
				</span>
			</ScrollAnimation>

			<ScrollAnimation 
				animateIn="fadeUp" 
				className="job-description"
				animateOnce={true}
			>
				{ elm.description }
			</ScrollAnimation>
		</div>
	)

    return (
        <div className="profile-section">
			<div className="container">

				<ScrollAnimation animateIn="fadeLeft" animateOnce={true}>	
					<h2 className="mg-heading mb-4">Sobre Mi</h2>
				</ScrollAnimation>
				
				<ScrollAnimation className="profile-description pb-5" animateIn="fadeUp" animateOnce={true} delay={100}>	
					Desarrollador Javascript Full-Stack con con mas de tres años de experiencia en desarrollo web front-end y estoy buscando adquirir mas experiencia en back-end. Enfocado en aprender, mejorar y superarme cada día, me gusta el trabajo en equipo, tomar nuevos retos y adquirir nuevos conocimientos. 
				</ScrollAnimation>

				<div className="d-flex flex-wrap trackline mt-sm-5 pt-5">
					{content}	
				</div>
			</div>
        </div>
    );
}

export default ProfileSection;
