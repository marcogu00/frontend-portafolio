import React, { useState } from "react";
import { useForm } from 'react-hook-form';
import {CSSTransition}  from 'react-transition-group';

import Client from '../../utils/Client';
import { ReactComponent as Close } from '../../assets/close.svg';
import { ReactComponent as Check } from '../../assets/check.svg';
import { ReactComponent as Error } from '../../assets/error.svg';

function ContactForm() {
    
    const emailRegexp = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    const [showAlert, setAlertState] = useState(false);
    const [loading, setLoadingState] = useState(false);
    const [ formStatus, setFormStatus] = useState({message: '', error: false});
    const { register, errors, handleSubmit, reset } = useForm(); 
    
    const onSubmit = (e) => {
        
        setLoadingState(true);
        setAlertState(false);
        setFormStatus({message: '', error: false});

        Client.post('/contact', e)
        .then(response => {
            
            reset();
            setLoadingState(false);
            setFormStatus({
                message: 'Gracias por contactarme !! Pronto me pondre en contacto contigo.',
                error: false
            });

            setAlertState(true);
            
        }).catch(error => {

            setFormStatus({
                message: 'Ocurrió un error al enviar el formuario, por favor intente de nuevo',
                error: true
            });

            setAlertState(true);

            setLoadingState(false);
            console.log(error);
        })
    }

    return (
        <form className="row" onSubmit={handleSubmit(onSubmit)}>

            <div className="col-md-6 mb-3 input-wrapper">
                <input className="form-control" type="text" name="name" ref={register({required: 'El campo es obligatorio.'})} />
                <label className="overlap-text" overlay-text="Nombre:">Nombre: </label>
                <p className="form-error">
                    {errors.name && errors.name.message}
                </p>
            </div>
            <div className="col-md-6 mb-3 input-wrapper">
                <input 
                    className="form-control" 
                    type="email" 
                    name="email" 
                    ref={register({
                        required: 'El campo es obligatorio.', 
                        pattern: {
                            value: emailRegexp,
                            message: 'La dirección de coreo es inválida.'
                        }
                    })} 
                />
                <label className="overlap-text" overlay-text="Email:">Email: </label>
                <p className="form-error">
                    {errors.email && errors.email.message}
                </p>
            </div>

            <div className="col-12 input-wrapper">
                <textarea className="form-control" name="message" ref={register({required: 'El campo es obligatorio.'})}>
                </textarea>
                
                <label className="overlap-text" overlay-text="Mensaje:">Mensaje: </label>
                <p className="form-error">
                    {errors.message && errors.message.message}
                </p>
            </div>
            
            <button className={`ml-3 btn dark ${(loading) ? 'disabled' : ''}`}>
                { (loading) ? (
                    
                    <div className="spinner-border text-light" role="status">
                        <span class="sr-only">Loading...</span>
                    </div> ) 
                
                    : 'Enviar'
                }
            </button>

            <CSSTransition
                mountOnEnter={true}
                unmountOnExit={true}
                in={showAlert}
                classNames="shrink-fade"
                timeout={800}
            >
                <div className="col-12 mt-4" >

                    <div className={`alert alert-${ formStatus.error ? 'danger' : 'success'}`}>
                        { (formStatus.error) ? <Error className="mr-2" /> : <Check className="mr-2" />}

                        { formStatus.message }
                        
                        <div onClick={() => setAlertState(false)} className="close ml-auto">
                            <Close />
                        </div>
                    </div>
                </div>
            </CSSTransition>
        </form>
    );
}

export default ContactForm;
