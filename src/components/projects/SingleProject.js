import React from "react";
import { Link } from "react-router-dom";
import './SingleProject.scss';
import SplitText from '../../utils/SplitText';
import ResponsiveImage from '../global/ResponsiveImage';
import ScrollAnimation from 'react-animate-on-scroll';
import { ReactComponent as Arrow } from '../../assets/arrow-right.svg';

class SingleProject extends React.Component {

    constructor(props){
        super(props);
        this.state = { 
            offset: 0
        }
    }
    render(){
        const { project } = this.props ;
        return (
            <div className="single-project">

                <ResponsiveImage img={project.img} lazy={true} /> 

                <div className="container d-flex flex-wrap align-content-center align-items-center h-100">
                    <Link to={'/proyecto/'+project._id} className="d-flex align-items-center w-100">
                        
                        <ScrollAnimation 
                            animateIn="active" 
                            className="chars-effect"
                            animateOnce={false}
                        >
                            <SplitText text={project.title} />
                        </ScrollAnimation>

                        <ScrollAnimation 
                            animateIn="fadeLeft" 
                            className="ml-auto ml-sm-0"
                            animateOnce={false}
                            delay={250}
                        >
                            <p className="align-items-center arrow-button d-flex justify-content-center mb-0 rounded-circle">
                                <Arrow />
                            </p>
                        </ScrollAnimation>


                    </Link>
                    <ScrollAnimation 
                        animateIn="fade" 
                        animateOnce={false}
                        duration={0.4}
                        delay={150}
                    >
                        <span className="project-year">
                            {project.year}
                        </span>
                    </ScrollAnimation>
                </div>

            </div>
        );
    }
}

export default SingleProject;
