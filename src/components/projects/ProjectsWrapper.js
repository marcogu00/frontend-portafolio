import React from "react";
import Loader from '../global/loaders/Loader';
import Client from '../../utils/Client';
import SingleProject from './SingleProject.js';

class ProjectsWrapper extends React.Component {

    constructor(props){
        super(props);
        this.state = { 
            loading: true,
            projects: []
        }
    }
    
    componentDidMount(){
        Client.get('/api/projects')
        .then(response => {
            
            response.data.data.sort((a, b) => a.order - b.order);

            this.setState({projects: response.data.data, loading: false});
            
        }).catch(error => {
            this.setState({loading: false});
            console.log(error);
        })
    }

    render(){
        
        if(this.state.loading){
            return ( <Loader width="100%" height="80vh" /> )
        }

        return (
            <div className="projects-wrapper">
                {this.state.projects.map(elm => 
                    <SingleProject project={elm} key={elm._id} />
                )}
            </div>
        );
    }
}

export default ProjectsWrapper;
