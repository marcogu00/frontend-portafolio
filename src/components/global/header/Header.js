import React from "react";
import { Link } from 'react-router-dom';
import config from '../../../utils/config';
import './Header.scss';
import SplitText from '../../../utils/SplitText';


// import { ReactComponent as Logo } from '../../../assets/logo.png';

class Header extends React.Component {

    constructor(props){
        super(props);
        this.state ={ isMounted: false, open: false }

        this.toggleMenu = this.toggleMenu.bind(this);
    }

    componentDidMount(){
        setTimeout(() => {
            this.setState({isMounted: true});
        }, 300);
    }

    toggleMenu(){
        this.setState(state => ({
            open: !state.open
        }))
    }

    render(){
        return (
            <header className={this.state.isMounted ? 'active site-header' : 'site-header'}>
                
                <Link to="/">
                    <img src={process.env.PUBLIC_URL + '/logo.png'} alt="logo" className="logo mr-auto" />
                    {/* <Logo className="logo mr-auto" />    */}
                </Link>
                
                <div 
                    className={'menu-toggler rounded-circle ' + (this.state.open ? 'open' : 'closed')} 
                    onClick={this.toggleMenu}
                >
                    <span></span>
                </div>

                <div className={'mega-menu ' + (this.state.open ? 'open' : '') } >
                    <div className="menu-open-layer"></div>
                    <div className="container h-100">
                        <div className="row h-100">
                            <ul className="navbar-nav col-lg-8 position-relative">
                                <span className="floating-menu-text">
                                    MENU
                                </span>
                                <li className="mr-4 pr-2">
                                    <Link 
                                        className="overlap-text on-hover"
                                        overlay-text="Home"
                                        onClick={this.toggleMenu}
                                        to="/" 
                                    >
                                        Home
                                    </Link>
                                </li>
                                <li className="mr-4 pr-2">
                                    <Link 
                                        className="overlap-text on-hover"
                                        overlay-text="Portafolio"
                                        onClick={this.toggleMenu}
                                        to="/portafolio"
                                    >
                                        Portafolio
                                    </Link>
                                </li>
                                <li>
                                    <Link 
                                        className="overlap-text on-hover"
                                        overlay-text="Contáctame"
                                        onClick={this.toggleMenu}
                                        to="/contacto"
                                    >
                                        Contáctame
                                    </Link>
                                </li>
                            </ul>
                            
                            <div className="align-items-start col-lg-4 d-flex flex-column mt-auto pb-5 menu-contact-info">
                                <a href={`mailto:${config.EMAIL}`} className={'chars-effect fancy-link ' + (this.state.open ? 'active' : '')}>
                                    <SplitText  text={config.EMAIL} />
                                </a>

                                <a href={`callto:${config.PHONE}`} className={'chars-effect fancy-link ' + (this.state.open ? 'active' : '')}>
                                    <SplitText  text={config.PHONE} />
                                </a>

                                <div className={'chars-effect mt-2 mb-3 ' + (this.state.open ? 'active' : '')}>
                                    <SplitText  text="Santiago, Región Metropolitanta, Chile." />
                                </div>

                                <ul className="list-inline">
                                    <li>
                                        <a href={config.LINKEDIN} target="_blank" rel="noopener noreferrer" >
                                            <svg width="17" height="16">
                                                <path d="M17.01 9.826v6.165H13.4v-5.755c0-1.447-.52-2.43-1.82-2.43a1.969 1.969 0 0 0-1.85 1.307 2.325 2.325 0 0 0-.13.872v6H6s.04-9.743 0-10.752h3.6v1.524a.072.072 0 0 1-.02.037h.02v-.037a3.6 3.6 0 0 1 3.26-1.776c2.37.005 4.15 1.541 4.15 4.845zM1.99.009a1.99 1.99 0 1 1 .05 3.978h-.02A1.99 1.99 0 1 1 1.99.009zM4.02 15.99H-.01v-11h4.03v11z">
                                                </path>
                                            </svg>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </header>
        )
    }
}

export default Header;
