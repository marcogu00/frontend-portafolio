import React, { useEffect } from "react";
import config from '../../utils/config';
import observeNewImage from '../../utils/LazyLoad';

function ResponsiveImage(props) {
    let image = null;

    useEffect(() => {
        if(props.lazy) {
            observeNewImage(image);
        }
    }, []);

    return (
        <picture className={props.className || ''} ref={(img) => { image = img; }}>
            <source 
                srcSet={ (props.lazy) ? '' : `${config.URL_API}${props.img.mobile}`}
                data-srcset={`${config.URL_API}${props.img.mobile}`}
            />
            <source 
                media="(min-width: 576px)"
                srcSet={ (props.lazy) ? '' : `${config.URL_API}${props.img.tablet}`}
                data-srcset={`${config.URL_API}${props.img.tablet}`}
            />
            <source 
                media="(min-width: 1200px)"
                srcSet={ (props.lazy) ? '' : `${config.URL_API}${props.img.desktop}`}
                data-srcset={`${config.URL_API}${props.img.desktop}`}
            />

            <img  
                src={ (props.lazy) ? '/placeholder.jpg' : `${config.URL_API}${props.img.desktop}`} 
                alt={props.alt || ''} 
                className="img-fluid" 
            />
        </picture>
    );
}

export default ResponsiveImage;
