import React from "react";
import { Link } from 'react-router-dom';
import config from '../../../utils/config';
import './Footer.scss';
import ScrollAnimation from 'react-animate-on-scroll';

function Footer() {
    return (
        <footer className="site-footer position-relative">
            <div className="container">
                <a href="/" className="footer-scroll-link position-absolute"> 
                    SCROLL
                </a>

                <div className="row">

                    <div className="col-lg-6">
                        <ScrollAnimation animateIn="fadeLeft" animateOnce={false} offset={20}>	
                            <h3 className="footer-logo">
                                Marco Gutierrez
                            </h3>
                        </ScrollAnimation>
                        
                        <ScrollAnimation animateIn="fadeUp" animateOnce={false} delay={200} offset={20} className="mt-3 pb-3 pb-lg-5">	
                            <p className="footer-logo-subheading">
                                Desarrollador Js Full-Stack
                            </p>
                        </ScrollAnimation>


                        <ScrollAnimation animateIn="fadeIn" animateOnce={false} offset={0} className="mt-lg-5 pt-lg-5">	
                            <a href={config.LINKEDIN} target="_blank" rel="noopener noreferrer" className="footer-rss-link">
                                <svg width="17" height="16">
                                    <path d="M17.01 9.826v6.165H13.4v-5.755c0-1.447-.52-2.43-1.82-2.43a1.969 1.969 0 0 0-1.85 1.307 2.325 2.325 0 0 0-.13.872v6H6s.04-9.743 0-10.752h3.6v1.524a.072.072 0 0 1-.02.037h.02v-.037a3.6 3.6 0 0 1 3.26-1.776c2.37.005 4.15 1.541 4.15 4.845zM1.99.009a1.99 1.99 0 1 1 .05 3.978h-.02A1.99 1.99 0 1 1 1.99.009zM4.02 15.99H-.01v-11h4.03v11z">
                                    </path>
                                </svg>
                            </a>
                        </ScrollAnimation>
                    </div>

                    <div className="col-lg-6 mt-5 pt-5 mt-lg-0 pt-lg-0 ">
                        <ScrollAnimation animateIn="fadeUp" animateOnce={false} offset={20}>	
                            <h2 className="mg-heading mb-4">CONTACTAME</h2>
                        </ScrollAnimation>
                        
                        <ScrollAnimation animateIn="fadeUp" animateOnce={false} delay={200} className="mb-5 pb-4" offset={20}>	
                            <a href={`mailto:${config.EMAIL}`} className="fancy-link">
                                {config.EMAIL}
                            </a><br></br>

                            <a href={`callto:${config.PHONE}`} className="fancy-link">
                                {config.PHONE}
                            </a>
                        </ScrollAnimation>
                        
                        <ScrollAnimation animateIn="fadeUp" animateOnce={false} delay={200} offset={20}>	
                            <h2 className="mg-heading mb-4">EXPLORA</h2>
                        </ScrollAnimation>
                        
                        <ScrollAnimation animateIn="fadeUp" animateOnce={false} delay={300} offset={20}>
                            <ul className="list-inline d-flex">
                                <li className="mr-4">
                                    <Link to="/portafolio">
                                        Portafolio
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/contacto">
                                        Contáctame
                                    </Link>
                                </li>
                            </ul>	
                        </ScrollAnimation>

                        <ScrollAnimation animateIn="fadeUp" animateOnce={false} delay={400} offset={20}>	
                            <p>Santiago, Región Metropolitanta, Chile.</p>
                        </ScrollAnimation>
                    </div>
                </div>
            </div>
        </footer>
    );
}

export default Footer;
