import React from "react";
import ResponsiveImage from '../ResponsiveImage';
import './HeroImage.scss';

class HeroImage extends React.Component {

    constructor(props){
        super(props);
        this.banner = React.createRef();
        
        let options = {
            root: null,
            rootMargin: '0px',
            threshold: 0
        }

        this.state = { 
            offset: 0,
            opacity: 1,
            observer: new IntersectionObserver(this.callback, options),
            id: null,
            project: null
        }
    }

    callback = (entries, observer) => {
        entries.forEach(element => {
            
            if(!element.isIntersecting){
                document.removeEventListener('scroll', this.handleScroll);
            }else{
                document.addEventListener('scroll', this.handleScroll);
            }
        });
    }

    handleScroll = (event) => {
        let opacity = (100 - ((window.pageYOffset / window.innerHeight) * 100)) * 0.01;
        let offset = ((window.pageYOffset / window.innerHeight) * 40);

        this.setState({opacity: opacity, offset: offset });
    }

    componentDidMount() {
        this.state.observer.observe(this.banner.current);
    }

    componentWillUnmount(){
        this.state.observer.disconnect();
        document.removeEventListener('scroll', this.handleScroll);
    }

    render(){
        const showImage = (this.props.showImage != undefined) ? this.props.showImage : true; 
        const { img, className } = this.props; 

        return (
            <div className={`hero-image ${className}`} ref={this.banner}>

                { showImage === true && (
                    <ResponsiveImage img={img} lazy={true} className="" /> 
                )}
    
                <div className="content-box d-flex flex-wrap align-items-end pb-4 pb-md-0 align-items-md-center">
    
                    <div 
                        className="col-12 col-sm-11 col-lg-10 col-xl-8 mx-auto"
                        style={{
                            opacity: `${this.state.opacity}`,
                            transform: `translateY(${this.state.offset}vh` 
                        }}
                    >
                        {this.props.children}
                        
                    </div>   
                </div>
            </div>
        );
    }
}

export default HeroImage;
