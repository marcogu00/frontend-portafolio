import React, { useEffect, useState } from "react";
import './Loader.scss';

function Loader(props) {

    const [computedWidth, setComputedWidth] = useState(null);
    const [options, setOptions] = useState({});

    useEffect(() => {
        setOptions({
            height: props.height || '1em',
            maxWidth: props.maxWidth || 100,
            minWidth: props.minWidth || 80,
        })

        setComputedWidth(props.width || `${Math.floor((Math.random() * (options.maxWidth - options.minWidth)) + options.minWidth)}%`)
    }, []);

    return (
        <span
            style={{ height: options.height, width: computedWidth }}
            className="loader"
        />
    )
}

export default Loader;