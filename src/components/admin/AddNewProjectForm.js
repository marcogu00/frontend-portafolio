import React, { useState } from "react";
import Client from '../../utils/Client';
import { useForm } from 'react-hook-form'

function AddNewProjectForm() {

    const { register, errors, handleSubmit, reset } = useForm(); 
    
    const galleryImg = [ {img: { order: '', file: '' }} ];

    const [galleryState, setGalleryState] = useState([
        {...galleryImg}
    ]);
    
    const [loading, setLoadingState] = useState(false);


    const addImageToGallery = () => {
        setGalleryState([...galleryState, {...galleryImg}]);
    }

    const onSubmit = (e) => {

        setLoadingState(true);

        let fd = new FormData();
        
        for (let i = 0; i < e.img.length; i++) {
            fd.append('img', e.img[i]);   
        }

        for (let i = 0; i < e.secondary_img.length; i++) {
            fd.append('secondary_img', e.secondary_img[i]);   
        }
        
        for (let i = 0; i < e.gallery.length; i++) {

            e.gallery[i].filename = e.gallery[i].file[0].name;
            e.gallery[i].file[0].order = e.gallery[i].order;
            fd.append('gallery', e.gallery[i].file[0]);   
        }

        fd.append('gallery_order', JSON.stringify(e.gallery));   

        for(let elm in e){

            if(elm !== 'gallery' && elm !== 'img' && elm !== 'technologies'){
                fd.append(elm, e[elm]);
            }
            
            if(elm === 'technologies'){
                fd.append(elm, JSON.stringify(e[elm]));
            }
        }

        Client.post('/api/projects', fd, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then(response => {
            reset();
            alert('Datos Guardados');
            console.log(response);
            setLoadingState(false);

        }).catch(error => {
            alert('Ocurrio un error al guardar los datos');
            setLoadingState(false);
            console.log(error);
        })
    }

    return (
        <div className="add-new-project-form">
            <form onSubmit={handleSubmit(onSubmit)}>

                <label>Titulo: </label>
                <input className="form-control" type="text" name="title" ref={register({required: 'El campo es obligatorio.'})} />
                <p>
                    {errors.title && errors.title.message}
                </p>
                
                <div class="custom-control custom-checkbox mb-3">
                    <input type="checkbox" class="custom-control-input" id="featuredProject" name="featured" ref={register()} />
                    <label class="custom-control-label" for="featuredProject">Destacado?</label>
                </div>
                
                <label>Orden: </label>
                <input className="form-control" type="text" name="order" ref={register({required: 'El campo es obligatorio.'})} />
                <p>
                    {errors.short_desc && errors.short_desc.message}
                </p>

                <label>Descripción corta: </label>
                <textarea className="form-control" name="short_desc" ref={register({required: 'El campo es obligatorio.'})} >
                </textarea>
                <p>
                    {errors.short_desc && errors.short_desc.message}
                </p>

                <label>Descripción: </label>
                <input className="form-control" type="text" name="description" ref={register({required: 'El campo es obligatorio.'})} />
                <p>
                    {errors.description && errors.description.message}
                </p>
                
                <label>Tecnologias: </label>
                <select className="custom-select" name="technologies" multiple ref={register({required: 'El campo es obligatorio.'})} > 
                    <option value="VueJs">VueJs</option>
                    <option value="Laravel">Laravel</option>
                    <option value="PHP">PHP</option>
                    <option value="AngularJs">AngularJs</option>
                    <option value="React">React</option>
                    <option value="Node">Node</option>
                    <option value="Sass">Sass</option>
                </select>

                <p>
                    {errors.technologies && errors.technologies.message}
                </p>
                
                <label>Año: </label>
                <input className="form-control" type="text" name="year" ref={register({required: 'El campo es obligatorio.'})} />
                <p>
                    {errors.year && errors.year.message}
                </p>
                
                <label>Diseñador: </label>
                <input className="form-control" type="text" name="designer" ref={register({required: 'El campo es obligatorio.'})} />
                <p>
                    {errors.designer && errors.designer.message}
                </p>
                
                <label>Url de Portafolio de Diseñador: </label>
                <input className="form-control" type="text" name="designer_portfolio" ref={register({required: 'El campo es obligatorio.'})} />
                <p>
                    {errors.designer_portfolio && errors.designer_portfolio.message}
                </p>

                <label>Url del proyecto: </label>
                <input className="form-control" type="text" name="url" ref={register({required: 'El campo es obligatorio.'})} />
                <p>
                    {errors.url && errors.url.message}
                </p>

                <label>Imagen Principal: </label>
                <div className=" border rounded p-4">
                    <input 
                        required
                        type="file"  
                        name="img" 
                        ref={register({required: 'El campo es obligatorio.'})}
                    />

                    <p className="mb-0">
                        {errors.img && errors.img.message}
                    </p>
                </div>
                
                <label>Imagen Secundaria: </label>
                <div className=" mt-3 border rounded p-4">
                    <input 
                        required
                        type="file"  
                        name="secondary_img" 
                        ref={register({required: 'El campo es obligatorio.'})}
                    />

                    <p className="mb-0">
                        {errors.secondary_img && errors.secondary_img.message}
                    </p>
                </div>
                
                
                <div className="row">
                    {galleryState.map((elm, i) =>  (
                            <div key={`gallery-${i}`} className="col-md-4 col-xl-3 mb-4">
                                <h3>Imagen de galeria #{i+1}</h3>

                                <label>Orden: </label>
                                <input 
                                    className="form-control" 
                                    required
                                    type="text" 
                                    name={`gallery[${i}][order]`} 
                                    ref={register({required: 'El campo es obligatorio.'})} 
                                />

                                <div className=" mt-3 border rounded p-4">
                                    <input 
                                        required
                                        type="file"  
                                        name={`gallery[${i}][file]`} 
                                        id={`gallery[${i}][file]`}
                                        data-idx={i}
                                        ref={register()}
                                    />
                                </div>
                            </div>
                        ))
                    }
                </div>
                
                <button className="btn btn-info" type="button" onClick={addImageToGallery}>
                    Agregar imagen
                </button>

                <button className={`btn btn-primary ${(loading) ? 'disabled' : ''}`}>
                    { (loading) ? 'Cargando...' : 'Guardar'}
                </button>


            </form>
        </div>
    );
}

export default AddNewProjectForm;
