import React from "react";
import Client from '../../utils/Client';
import { useForm } from 'react-hook-form'

function LoginForm() {

    const { register, errors, handleSubmit } = useForm(); 

    const onSubmit = (e) => {

        let data = new FormData();
        data.append('email', e.email);
        data.append('password', e.password);

        Client.post('/api/auth', e)
        .then(response => {
            
        }).catch(error => {
            console.log(error);
        })
    }

    return (
        <div className="add-new-project-form">
            <form onSubmit={handleSubmit(onSubmit)} >
                
                <label>Email: </label>
                <input type="text" name="email" ref={register({required: 'El campo es obligatorio.'})} />
                <p>
                    {errors.title && errors.title.message}
                </p>
                
                <label>Password: </label>
                <input type="password" name="password" ref={register({required: 'El campo es obligatorio.'})} />
                <p>
                    {errors.password && errors.password.message}
                </p>

                <button className="btn btn-primary">
                    Guardar
                </button>
            </form>
        </div>
    );
}

export default LoginForm;
